package cn.codeyang.service.impl;

import cn.codeyang.domain.User;
import cn.codeyang.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * Created by meicai on 2016/6/27
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:beans.xml")
public class UserServiceImplTest {

    @Resource
    private UserService userService;

    @Test
    public void testRegister() throws Exception {
//        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:beans.xml");
//        UserService userService = (UserService) context.getBean("userServiceImpl");
        User user = new User();
        user.setLoginname("yangzhongyang");
        user.setLoginpass("123");
        System.out.println(userService.register(user));
    }

    @Test
    public void test(){
        System.out.println("123");
    }
}