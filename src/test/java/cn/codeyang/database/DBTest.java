package cn.codeyang.database;

import cn.codeyang.dao.UserDao;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.sql.DataSource;

/**
 * Created by meicai on 2016/6/27
 */
public class DBTest {
    @Test
    public void testDb(){
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:beans.xml");
        DataSource dataSource = (DataSource) context.getBean("dataSource");
        System.out.println(dataSource);
    }

    @Test
    public void test2(){
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:beans.xml");
        UserDao userDao = (UserDao) context.getBean("userDaoImpl");
        System.out.println(userDao);
    }
}
