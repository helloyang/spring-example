package cn.codeyang.service;

import cn.codeyang.domain.User;

/**
 * Created by meicai on 2016/6/27
 */
public interface UserService {
    boolean register(User user);
}
