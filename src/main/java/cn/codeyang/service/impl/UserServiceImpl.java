package cn.codeyang.service.impl;

import cn.codeyang.dao.UserDao;
import cn.codeyang.domain.User;
import cn.codeyang.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by meicai on 2016/6/27
 */
@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserDao userDao;

    @Override
    public boolean register(User user) {
        return userDao.insert(user) > 0;
    }
}
