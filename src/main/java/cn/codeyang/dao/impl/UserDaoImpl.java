package cn.codeyang.dao.impl;

import cn.codeyang.dao.BaseDao;
import cn.codeyang.dao.UserDao;
import cn.codeyang.domain.User;
import org.springframework.stereotype.Repository;

/**
 * Created by meicai on 2016/6/27
 */
@Repository
public class UserDaoImpl extends BaseDao implements UserDao {

    String ns = "cn.codeyang.domain.UserMapper.";

    @Override
    public int insert(User user) {
        return this.getSqlSession().insert(ns + "insert", user);
    }
}
