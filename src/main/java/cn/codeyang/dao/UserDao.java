package cn.codeyang.dao;

import cn.codeyang.domain.User;

/**
 * Created by meicai on 2016/6/27
 */
public interface UserDao {
    int insert(User user);
}
